#### Microblog
This is a API REST Backend about *microblog* system like Twitter

#### TASK
- [x] users
- [x] posts
- [x] comments
- [x] followers
- [ ] do apply security framework
- [ ] exceptions
- [ ] implement logging service
- [ ] implement service layer
- [ ] implement repository pattern
- [ ] refactory tests with mock
- [ ] try flask-rest

#### Project structure
```
+- microblog/
	+- controllers/
	+- dao/
	+- exceptions/
	+- models/
	+- tests/
```

#### Techlonogies
- python3
- flask
- sqlite3
- unittest
- black
- flake8

#### Pre-requerimets
You must had installed the follow apps
- git 2.39
- python 3.11
- sqlite 3

#### Download, install dependencies and execute
```
git clone git@gitlab.com:luciano.sl.arg/microblog.git
cd microblog
. venv/bin/activate
pip install -r requirements-dev.txt
flask --app microblog run
```

#### Execute tests
```
python3 -munittest discover -s microblog/tests/ -p 'test_*.py'
```

#### Test API with curl
```
curl -L 'http://127.0.0.1:5000/api/users' -H 'content-type: application/json'
```

#### About the tests
We needs (enough) tests to ensure quality™

Tests are following:

- functional tests
- rejection tests
- exception tests
- especial cases

#### Resources
[Flask Documentation 3.x](https://flask.palletsprojects.com/en/3.0.x/)

#### Donations?
Just find me out
