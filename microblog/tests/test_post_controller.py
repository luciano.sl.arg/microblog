import unittest

from microblog import create_app
from microblog.dao import PostDao, UserDao
from microblog.tests import utils


class TestPostController(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()
        self.app.testing = True

    def test_create_post(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        data = utils.generate_post_data(user_saved.id)
        res = self.client.post("/api/posts", follow_redirects=True, json=data)

        self.assertEqual(res.status_code, 201)
        self.assertEqual(res.json["content"], data["content"])

    def test_get_all_posts(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        post = utils.create_post(user_saved.id)
        PostDao.insert(post)

        res = self.client.get("/api/posts", follow_redirects=True)

        self.assertEqual(res.status_code, 200)

    def test_get_one_post(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        post = utils.create_post(user_saved.id)
        post_saved = PostDao.insert(post)

        res = self.client.get(
            f"/api/posts/{post_saved.id}", follow_redirects=True
        )

        self.assertEqual(res.status_code, 200)

    def test_update_post(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        post = utils.create_post(user_saved.id)
        post_saved = PostDao.insert(post)

        dataNew = utils.generate_post_data(user_saved.id)
        res = self.client.put(
            f"/api/posts/{post_saved.id}", follow_redirects=True, json=dataNew
        )

        self.assertEqual(res.status_code, 200)
        self.assertNotEqual(post_saved.content, res.json["content"])

    def test_delete_post(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        post = utils.create_post(user_saved.id)
        post_saved = PostDao.insert(post)
        res = self.client.delete(
            f"/api/posts/{post_saved.id}", follow_redirects=True
        )

        self.assertEqual(res.status_code, 204)

    def test_not_found_get_by_id(self):
        res = self.client.get("/api/posts/2147483647", follow_redirects=True)

        self.assertEqual(res.status_code, 404)

    def test_not_found_update(self):
        data = utils.generate_post_data(2147483647)
        res = self.client.put(
            "/api/posts/2147483647", follow_redirects=True, json=data
        )

        self.assertEqual(res.status_code, 404)

    # WARNING: Sqlite3 don't raise Exception so we will get 204 anyway
    def test_not_found_delete(self):
        res = self.client.delete(
            "/api/posts/2147483647", follow_redirects=True
        )

        self.assertEqual(res.status_code, 204)

    # WARNING: Sqlite3 does not check if the foreign key is valid when inserted
    # The check is done in the controllers
    def test_foreign_key_exeption_on_create(self):
        data = utils.generate_post_data(2147483647)
        res = self.client.post("/api/posts/", follow_redirects=True, json=data)

        self.assertEqual(res.status_code, 400)


if __name__ == "__main__":
    unittest.main()
