import sqlite3
import unittest

from microblog.dao import UserDao
from microblog.models import User
from microblog.tests import utils


class TestUserDao(unittest.TestCase):
    test_fields = [
        "firstname",
        "secondname",
        "lastname",
        "address",
        "city",
        "phone",
        "email",
    ]

    def test_insert(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        self.assertIsNotNone(user_saved)
        self.assertIsNotNone(user_saved.id)
        self.assertIsNotNone(user_saved.created)
        self.assertTrue(
            all(
                getattr(user_saved, key) == getattr(user, key)
                for key in self.test_fields
            )
        )

    def test_get_by_id(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)

        user_found = UserDao.get_by_id(user_saved.id)

        self.assertIsNotNone(user_found)
        self.assertIsInstance(user_found, User)
        self.assertTrue(
            all(
                getattr(user_found, key) == getattr(user_saved, key)
                for key in self.test_fields
            )
        )

    def test_get_all(self):
        user = utils.create_user()
        UserDao.insert(user)
        users = UserDao.get_all()
        self.assertIsNotNone(users)
        self.assertIsInstance(users, list)

    def test_update(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)

        user = utils.create_user()
        user.id = user_saved.id
        user_updated = UserDao.update(user)
        self.assertIsNotNone(user_updated)
        self.assertIsNotNone(user_updated.updated)
        self.assertTrue(
            all(
                getattr(user_updated, k) != getattr(user_saved, k)
                for k in self.test_fields
            )
        )

    def test_delete(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)

        UserDao.delete_by_id(user_saved.id)

        user_found = UserDao.get_by_id(user_saved.id)
        self.assertIsNone(user_found)

    def test_not_found_get_by_id(self):
        user_saved = UserDao.get_by_id(2147483647)
        self.assertIsNone(user_saved)

    def test_not_found_update(self):
        user_new = utils.create_user()
        user_new.id = 2147483647
        user_saved = UserDao.update(user_new)
        self.assertIsNone(user_saved)

    def test_not_found_delete(self):
        user_new = utils.create_user()
        user_new.id = 2147483647
        user_deleted = UserDao.delete_by_id(user_new.id)
        self.assertIsNone(user_deleted)

    def test_insert_trigger_unique_exception(self):
        user_new = utils.create_user()
        user_saved = UserDao.insert(user_new)
        with self.assertRaises(sqlite3.IntegrityError):
            UserDao.insert(user_saved)

    def test_update_trigger_unique_exception(self):
        user_new1 = utils.create_user()
        user_saved1 = UserDao.insert(user_new1)
        user_new2 = utils.create_user()
        user_saved2 = UserDao.insert(user_new2)
        user_saved2.email = user_saved1.email
        with self.assertRaises(sqlite3.IntegrityError):
            UserDao.update(user_saved2)


if __name__ == "__main__":
    unittest.main()
