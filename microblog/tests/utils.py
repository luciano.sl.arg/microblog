import sqlite3

import faker

from microblog import config
from microblog.models import Comment, Follower, Post, User

fake = faker.Faker("es_AR")


def generate_user_data():
    return dict(
        firstname=fake.first_name(),
        secondname=fake.first_name(),
        lastname=fake.last_name(),
        birth=fake.date_of_birth().isoformat(),
        address=fake.street_address(),
        city=fake.city(),
        phone=fake.random_int(0, 2**63 - 1),
        email=fake.safe_email(),
        password=fake.password(special_chars=False, upper_case=False),
    )


def generate_post_data(user_id):
    return dict(author=user_id, content=fake.paragraph())


def generate_comment_data(user_id, post_id):
    return dict(author=user_id, post=post_id, content=fake.paragraph())


def generate_follower_data(user1_id, user2_id):
    return dict(following=user1_id, followed=user2_id)


def create_user():
    return User(**generate_user_data())


def create_post(user_id):
    return Post(author=user_id, content=fake.paragraph())


def create_comment(user_id, post_id):
    return Comment(author=user_id, post=post_id, content=fake.paragraph())


def create_follower(user_from, user_to):
    return Follower(following=user_from, followed=user_to)


def recreate_database():
    with sqlite3.connect(config.DATABASE) as con:
        with open(config.SCHEMA) as fp:
            con.executescript(
                """
                DROP TRIGGER IF EXISTS users_update;
                DROP TRIGGER IF EXISTS posts_update;
                DROP TRIGGER IF EXISTS comments_update;
                DROP TRIGGER IF EXISTS followers_update;
                DROP TABLE IF EXISTS followers;
                DROP TABLE IF EXISTS comments;
                DROP TABLE IF EXISTS posts;
                DROP TABLE IF EXISTS users;
                """
            )
            con.executescript(fp.read())
