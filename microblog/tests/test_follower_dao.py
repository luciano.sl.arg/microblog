import unittest

from microblog.dao import FollowerDao, UserDao
from microblog.models import Follower
from microblog.tests import utils


class TestFollowerDao(unittest.TestCase):
    def test_insert_follower(self):
        user_from = UserDao.insert(utils.create_user())
        user_to = UserDao.insert(utils.create_user())

        follower = FollowerDao.insert(Follower(user_from.id, user_to.id))
        follower = FollowerDao.get_by_id(follower.id)

        self.assertIsNotNone(follower)
        self.assertIs(follower.following, user_from.id)
        self.assertIs(follower.followed, user_to.id)

    def test_get_following_to(self):
        followers = []
        user_saved = UserDao.insert(utils.create_user())
        for _ in range(10):
            user_follower = utils.create_user()
            user_follower_saved = UserDao.insert(user_follower)
            followers.append(user_follower_saved)
            FollowerDao.insert(Follower(user_saved.id, user_follower_saved.id))

        followers_saved = FollowerDao.get_following_to(user_saved.id)

        self.assertIsNotNone(followers_saved)
        self.assertIsInstance(followers_saved, list)
        self.assertTrue(
            all(f.following == user_saved.id for f in followers_saved)
        )

    def test_get_followed_by(self):
        followers = []
        user_saved = UserDao.insert(utils.create_user())
        for _ in range(10):
            user_followed = utils.create_user()
            user_followed_saved = UserDao.insert(user_followed)
            followers.append(user_followed_saved)
            FollowerDao.insert(Follower(user_saved.id, user_followed_saved.id))

        followers_saved = FollowerDao.get_followed_by(user_saved.id)

        self.assertIsNotNone(followers_saved)
        self.assertIsInstance(followers_saved, list)
        self.assertTrue(
            all(f.followed == user_saved.id for f in followers_saved)
        )

    def test_get_follower(self):
        user_following = UserDao.insert(utils.create_user())
        user_followed = UserDao.insert(utils.create_user())

        _follower = FollowerDao.insert(
            Follower(user_following.id, user_followed.id)
        )

        follower = FollowerDao.get_by_id(_follower.id)

        self.assertIsNotNone(follower)
        self.assertTrue(user_following.id == follower.following)
        self.assertTrue(user_followed.id == follower.followed)

    def test_delete_follower(self):
        user_following_saved = UserDao.insert(utils.create_user())
        user_followed_saved = UserDao.insert(utils.create_user())
        follower_saved = FollowerDao.insert(
            Follower(user_following_saved.id, user_followed_saved.id)
        )

        FollowerDao.delete_by_id(follower_saved.id)

        followed_test = FollowerDao.get_by_id(user_followed_saved.id)
        self.assertIsNone(followed_test)


if __name__ == "__main__":
    unittest.main()
