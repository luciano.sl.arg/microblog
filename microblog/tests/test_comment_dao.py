import unittest

from microblog.dao import CommentDao, PostDao, UserDao
from microblog.models import Comment
from microblog.tests import utils


class TestCommentDao(unittest.TestCase):
    def setUp(self):
        user = utils.create_user()
        self.user = UserDao.insert(user)
        post = utils.create_post(user.id)
        self.post = PostDao.insert(post)

    def test_insert_comment(self):
        comment_new = utils.create_comment(self.user.id, self.post.id)
        comment_saved = CommentDao.insert(comment_new)

        self.assertIsInstance(comment_saved, Comment)
        self.assertIsNotNone(comment_saved.id)
        self.assertIsNotNone(comment_saved.created)
        self.assertEqual(comment_saved.content, comment_new.content)

    def test_get_by_id(self):
        comment_new = utils.create_comment(self.user.id, self.post.id)
        comment_saved = CommentDao.insert(comment_new)

        comment_found = CommentDao.get_by_id(comment_saved.id)

        self.assertIsInstance(comment_found, Comment)
        self.assertEqual(comment_found.content, comment_new.content)

    def test_get_all(self):
        comments = CommentDao.get_all()
        self.assertIsInstance(comments, list)

    def test_update(self):
        comment = utils.create_comment(self.user.id, self.post.id)
        comment_saved = CommentDao.insert(comment)
        comment_new = utils.create_comment(self.user.id, self.post.id)
        comment_new.id = comment_saved.id
        commentUpdated = CommentDao.update(comment_new)

        self.assertIsInstance(commentUpdated, Comment)
        self.assertIsNotNone(commentUpdated.updated)
        self.assertNotEqual(commentUpdated.content, comment_saved.content)

    def test_delete(self):
        comment = utils.create_comment(self.user.id, self.post.id)
        comment_saved = CommentDao.insert(comment)
        CommentDao.delete_by_id(comment_saved.id)

        comment_found = CommentDao.get_by_id(comment_saved.id)
        self.assertIsNone(comment_found)

    def test_not_found_get_by_id(self):
        comment_saved = CommentDao.get_by_id(2147483647)
        self.assertIsNone(comment_saved)

    def test_not_found_update(self):
        comment_new = utils.create_comment(self.user.id, self.post.id)
        comment_new.id = 2147483647
        comment_saved = CommentDao.update(comment_new)
        self.assertIsNone(comment_saved)

    def test_not_found_delete(self):
        comment_new = utils.create_comment(self.user.id, self.post.id)
        comment_new.id = 2147483647
        commentDeleted = CommentDao.delete_by_id(comment_new.id)
        self.assertIsNone(commentDeleted)

    @unittest.skip("SQLite ignore the ON CASCADE clause")
    def test_on_delete_cascade_clause_on_author(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        post = utils.create_post(user_saved.id)
        post_saved = PostDao.insert(post)
        author = utils.create_user()
        author_saved = UserDao.insert(author)
        comment = utils.create_comment(author_saved.id, post_saved.id)
        comment_saved = CommentDao.insert(comment)
        UserDao.delete_by_id(author_saved.id)
        comment_found = CommentDao.get_by_id(comment_saved.id)
        self.assertIsNone(comment_found)

    @unittest.skip("SQLite ignore the ON CASCADE clause")
    def test_on_delete_cascade_clause_on_post(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        post = utils.create_post(user_saved.id)
        post_saved = PostDao.insert(post)
        author = utils.create_user()
        author_saved = UserDao.insert(author)
        comment = utils.create_comment(author_saved.id, post_saved.id)
        comment_saved = CommentDao.insert(comment)
        PostDao.delete_by_id(post_saved.id)
        comment_found = CommentDao.get_by_id(comment_saved.id)
        self.assertIsNone(comment_found)


if __name__ == "__main__":
    unittest.main()
