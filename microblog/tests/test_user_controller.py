import unittest

from microblog import create_app
from microblog.dao import UserDao
from microblog.tests import utils


class TestUserController(unittest.TestCase):
    test_fields = [
        "firstname",
        "secondname",
        "lastname",
        "address",
        "city",
        "phone",
        "email",
    ]

    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()
        self.app.testing = True

        # ensure we have at least  one user, or this test will fail
        user = utils.create_user()
        self.user = UserDao.insert(user)

    def test_create_user(self):
        data = utils.generate_user_data()
        res = self.client.post("/api/users", follow_redirects=True, json=data)

        self.assertEqual(res.status_code, 201)

    def test_get_all_users(self):
        res = self.client.get("/api/users", follow_redirects=True)

        self.assertEqual(res.status_code, 200)

    def test_get_one_user(self):
        res = self.client.get("/api/users/1", follow_redirects=True)

        self.assertEqual(res.status_code, 200)

    def test_update_user(self):
        res = self.client.get("/api/users/1", follow_redirects=True)

        data_orig = res.json
        data = utils.generate_user_data()
        res = self.client.put("/api/users/1", follow_redirects=True, json=data)

        self.assertEqual(res.status_code, 200)
        data_updated = res.json

        self.assertIsNotNone(data_updated)
        self.assertIsNotNone(data_updated["updated"])
        self.assertIsNotNone(data_updated["id"])
        self.assertTrue(
            all(
                data_orig[key] != data_updated[key] for key in self.test_fields
            )
        )

    def test_delete_user(self):
        user = utils.create_user()
        userSaved = UserDao.insert(user)
        res = self.client.delete(
            f"/api/users/{userSaved.id}", follow_redirects=True
        )

        self.assertEqual(res.status_code, 204)

    def test_not_found_get_by_id(self):
        res = self.client.get("/api/users/2147483647", follow_redirects=True)

        self.assertEqual(res.status_code, 404)

    def test_not_found_update(self):
        data = utils.generate_user_data()
        res = self.client.put(
            "/api/users/2147483647", follow_redirects=True, json=data
        )

        self.assertEqual(res.status_code, 404)

    # WARNING: Sqlite3 don't raise Exception so we will get 204 anyway
    def test_not_found_delete(self):
        res = self.client.delete(
            "/api/users/2147483647", follow_redirects=True
        )

        self.assertEqual(res.status_code, 204)

    def test_insert_trigger_unique_exception(self):
        data = utils.generate_user_data()
        self.client.post("/api/users", follow_redirects=True, json=data)

        res = self.client.post("/api/users", follow_redirects=True, json=data)

        self.assertEqual(res.status_code, 400)

    def test_update_trigger_unique_exception(self):
        data1 = utils.generate_user_data()
        res1 = self.client.post(
            "/api/users", follow_redirects=True, json=data1
        )

        id1 = res1.json["id"]
        data2 = utils.generate_user_data()
        self.client.post("/api/users", follow_redirects=True, json=data2)

        res = self.client.put(
            f"/api/users/{id1}", follow_redirects=True, json=data2
        )

        self.assertEqual(res.status_code, 400)


if __name__ == "__main__":
    unittest.main()
