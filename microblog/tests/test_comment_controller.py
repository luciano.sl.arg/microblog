import unittest

from microblog import create_app
from microblog.dao import CommentDao, PostDao, UserDao
from microblog.tests import utils


class TestCommentController(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()
        self.app.testing = True
        author = utils.create_user()
        self.author = UserDao.insert(author)
        post = utils.create_post(self.author.id)
        user = utils.create_user()
        self.user = UserDao.insert(user)
        self.post = PostDao.insert(post)

    def test_create_comment(self):
        data = utils.generate_comment_data(self.user.id, self.post.id)
        res = self.client.post(
            "/api/comments", follow_redirects=True, json=data
        )

        self.assertEqual(res.status_code, 201)

    def test_get_all_comments(self):
        comment = utils.create_comment(self.user.id, self.post.id)
        CommentDao.insert(comment)

        res = self.client.get("/api/comments", follow_redirects=True)
        self.assertEqual(res.status_code, 200)

    def test_get_one_comment(self):
        comment = utils.create_comment(self.user.id, self.post.id)
        comment_saved = CommentDao.insert(comment)

        res = self.client.get(
            f"/api/comments/{comment_saved.id}", follow_redirects=True
        )

        self.assertEqual(res.status_code, 200)
        dataUpdated = res.json
        self.assertEqual(comment_saved.content, dataUpdated["content"])

    def test_update_comment(self):
        comment = utils.create_comment(self.user.id, self.post.id)
        comment_saved = CommentDao.insert(comment)

        data = utils.generate_comment_data(self.user.id, self.post.id)
        res = self.client.put(
            f"/api/comments/{comment_saved.id}",
            follow_redirects=True,
            json=data,
        )

        self.assertEqual(res.status_code, 200)
        dataUpdated = res.json
        self.assertNotEqual(comment_saved.content, dataUpdated["content"])

    def test_delete_comment(self):
        comment = utils.create_comment(self.user.id, self.post.id)
        comment_saved = CommentDao.insert(comment)
        res = self.client.delete(
            f"/api/comments/{comment_saved.id}", follow_redirects=True
        )

        self.assertEqual(res.status_code, 204)

    def test_not_found_get_by_id(self):
        res = self.client.get(
            "/api/comments/2147483647", follow_redirects=True
        )

        self.assertEqual(res.status_code, 404)

    def test_not_found_update(self):
        data = utils.generate_comment_data(self.user.id, self.post.id)
        res = self.client.put(
            "/api/comments/2147483647", follow_redirects=True, json=data
        )

        self.assertEqual(res.status_code, 404)

    # WARNING: Sqlite3 don't raise Exception so we will get 204 anyway
    def test_not_found_delete(self):
        res = self.client.delete(
            "/api/comments/2147483647", follow_redirects=True
        )

        self.assertEqual(res.status_code, 204)

    # WARNING: Sqlite3 does not check if the foreign key is valid when inserted
    # This check is done by controllers
    def test_foreign_key_exeption_on_create(self):
        data = utils.generate_comment_data(2147483647, 2147483647)
        res = self.client.post(
            "/api/comments/", follow_redirects=True, json=data
        )

        self.assertEqual(res.status_code, 400)


if __name__ == "__main__":
    unittest.main()
