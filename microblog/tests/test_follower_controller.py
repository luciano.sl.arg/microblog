import unittest

from microblog import create_app
from microblog.dao import FollowerDao, UserDao
from microblog.models import Follower
from microblog.tests import utils


class FollowerController(unittest.TestCase):
    def setUp(self):
        self.app = create_app()
        self.client = self.app.test_client()
        self.app.testing = True

    def test_insert_follower(self):
        user_following = UserDao.insert(utils.create_user())
        user_followed = UserDao.insert(utils.create_user())
        data = utils.generate_follower_data(
            user_following.id, user_followed.id
        )
        resp = self.client.post(
            "/api/followers/", follow_redirects=True, json=data
        )

        self.assertEqual(resp.status_code, 201)

    def test_detail(self):
        user_following = UserDao.insert(utils.create_user())
        user_followed = UserDao.insert(utils.create_user())

        _follower = Follower(user_following.id, user_followed.id)
        follower = FollowerDao.insert(_follower)

        resp = self.client.get(
            f"/api/followers/{follower.id}", follow_redirects=True
        )

        self.assertEqual(resp.status_code, 200)
        self.assertTrue(user_following.id == resp.json["following"])
        self.assertTrue(user_followed.id == resp.json["followed"])

    def test_detail_not_found(self):
        resp = self.client.get(
            "/api/followers/2147483647", follow_redirects=True
        )
        self.assertEqual(resp.status_code, 404)

    def test_get_following_to(self):
        followers = []
        user_saved = UserDao.insert(utils.create_user())
        for _ in range(10):
            user_follower = utils.create_user()
            user_follower_saved = UserDao.insert(user_follower)
            followers.append(user_follower_saved)
            FollowerDao.insert(Follower(user_saved.id, user_follower_saved.id))

        resp = self.client.get(
            f"/api/followers/following/{user_saved.id}", follow_redirects=True
        )
        self.assertEqual(resp.status_code, 200)

    def test_get_followed_by(self):
        followers = []
        user_saved = UserDao.insert(utils.create_user())
        for _ in range(10):
            user_follower = utils.create_user()
            user_follower_saved = UserDao.insert(user_follower)
            followers.append(user_follower_saved)
            FollowerDao.insert(Follower(user_follower_saved.id, user_saved.id))

        resp = self.client.get(
            f"/api/followers/followed/{user_saved.id}", follow_redirects=True
        )
        self.assertEqual(resp.status_code, 200)


if __name__ == "__main__":
    unittest.main()
