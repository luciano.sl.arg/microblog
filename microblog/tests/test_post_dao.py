import unittest

from microblog.dao import PostDao, UserDao
from microblog.models import Post
from microblog.tests import utils


class TestPostDao(unittest.TestCase):
    def setUp(self):
        user = utils.create_user()
        self.user = UserDao.insert(user)

    def test_insert_post(self):
        post_new = utils.create_post(self.user.id)
        post_saved = PostDao.insert(post_new)

        self.assertIsNotNone(post_saved)
        self.assertIsNotNone(post_saved.id)
        self.assertIsNotNone(post_saved.created)
        self.assertEqual(post_new.content, post_saved.content)

    def test_get_by_id(self):
        post_new = utils.create_post(self.user.id)
        post_saved = PostDao.insert(post_new)

        post_found = PostDao.get_by_id(post_saved.id)

        self.assertIsNotNone(post_found)
        self.assertIsInstance(post_found, Post)
        self.assertEqual(post_found.content, post_saved.content)

    def test_get_all(self):
        posts = PostDao.get_all()
        self.assertIsNotNone(posts)
        self.assertIsInstance(posts, list)

    def test_update(self):
        post = utils.create_post(self.user.id)
        post_saved = PostDao.insert(post)
        post_new = utils.create_post(self.user.id)
        post_new.id = post_saved.id
        post_updated = PostDao.update(post_new)

        self.assertIsNotNone(post_updated.updated)
        self.assertNotEqual(post_updated.content, post_saved.content)

    def test_delete(self):
        post = utils.create_post(self.user.id)
        post_saved = PostDao.insert(post)
        PostDao.delete_by_id(post_saved.id)

        post_found = PostDao.get_by_id(post_saved.id)
        self.assertIsNone(post_found)

    def test_not_found_get_by_id(self):
        post_saved = PostDao.get_by_id(2147483647)
        self.assertIsNone(post_saved)

    def test_not_found_update(self):
        post_new = utils.create_post(self.user.id)
        post_new.id = 2147483647
        post_saved = PostDao.update(post_new)
        self.assertIsNone(post_saved)

    def test_not_found_delete(self):
        post_new = utils.create_post(self.user.id)
        post_new.id = 2147483647
        post_deleted = PostDao.delete_by_id(post_new.id)
        self.assertIsNone(post_deleted)

    @unittest.skip("SQLite ignore the ON CASCADE clause")
    def test_on_delete_cascade_clause_on_author(self):
        user = utils.create_user()
        user_saved = UserDao.insert(user)
        post = utils.create_post(user_saved.id)
        post_saved = PostDao.insert(post)
        UserDao.delete_by_id(user_saved.id)
        post_found = PostDao.get_by_id(post_saved.id)
        self.assertIsNone(post_found)


if __name__ == "__main__":
    unittest.main()
