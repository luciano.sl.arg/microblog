import os

DEBUG = True
SECRET_KEY = os.getenv("SECRET_KEY") or "development key"
DATABASE = "db.sqlite3"
SCHEMA = "schema.sql"
