class Follower:
    def __init__(self, following, followed):
        self.id = None
        self.following = following
        self.followed = followed

    def __repr__(self):
        return f"""Follower(
                        id = {self.id},
                        following = {self.following},
                        followed = {self.followed}
                        )"""
