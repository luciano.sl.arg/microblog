class Comment:
    def __init__(self, author, post, content):
        self.id = None
        self.author = author
        self.post = post
        self.content = content
        self.updated = None
        self.created = None

    def __str__(self):
        return f"{self.content}"

    def __repr__(self):
        return f"""Comment(
                        id = {self.id},
                        author = {self.author},
                        post = {self.post},
                        content = {self.content},
                        updated = {self.updated},
                        created = {self.created}
                        )"""
