class User:
    def __init__(
        self,
        firstname,
        secondname,
        lastname,
        birth,
        address,
        city,
        phone,
        email,
        password,
    ):
        self.id = None
        self.firstname = firstname
        self.secondname = secondname
        self.lastname = lastname
        self.birth = birth
        self.address = address
        self.city = city
        self.phone = phone
        self.email = email
        self.password = password
        self.updated = None
        self.created = None

    def __str__(self):
        return f"{self.lastname}, {self.firstname} {self.secondname}"

    def __repr__(self):
        return f"""User(
                        id = {self.id},
                        firstname = {self.firstname},
                        secondname = {self.secondname},
                        lastname = {self.lastname},
                        birth = {self.birth},
                        address = {self.address},
                        city = {self.city},
                        phone = {self.phone},
                        email = {self.email},
                        password = {self.password},
                        updated = {self.updated},
                        created = {self.created}
                        )"""
