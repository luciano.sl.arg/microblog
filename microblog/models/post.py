class Post:
    def __init__(self, author, content):
        self.id = None
        self.author = author
        self.content = content
        self.updated = None
        self.created = None

    def __str__(self):
        return f"{self.content}"

    def __repr__(self):
        return f"""Post(
                        id = {self.id},
                        author = {self.author},
                        content = {self.content},
                        updated = {self.updated},
                        created = {self.created}
                        )"""
