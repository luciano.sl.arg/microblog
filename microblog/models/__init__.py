from microblog.models.comment import Comment
from microblog.models.follower import Follower
from microblog.models.post import Post
from microblog.models.user import User
