import logging
import os

from flask import Flask
from flask.logging import default_handler


def create_app():
    app = Flask(__name__)
    app.config.from_pyfile("config.py")
    logger = app.logger
    logger.removeHandler(default_handler)
    handler = logging.FileHandler("messages.log")
    handler.setFormatter(
        logging.Formatter(
            "[%(asctime)s] %(levelname)s in %(module)s: %(message)s"
        )
    )
    logger.addHandler(handler)

    from microblog.controllers import comment, follower, post, user

    app.register_blueprint(user.bp)
    app.register_blueprint(post.bp)
    app.register_blueprint(comment.bp)
    app.register_blueprint(follower.bp)

    return app
