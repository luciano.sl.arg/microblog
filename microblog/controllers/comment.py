from flask import Blueprint, abort, jsonify, request

from microblog.dao import CommentDao, PostDao, UserDao
from microblog.models import Comment

bp = Blueprint("comments", __name__, url_prefix="/api/comments")


@bp.get("/<int:id>")
def get_comment_by_id(id):
    comment = CommentDao.get_by_id(id)
    if not comment:
        return abort(404, description="Not Found")
    return jsonify(comment.__dict__), 200


@bp.get("/")
def list_comments():
    return jsonify([x.__dict__ for x in CommentDao.get_all()]), 200


@bp.post("/")
def create_comment():
    data = request.get_json()
    comment = Comment(**data)

    if not UserDao.get_by_id(data.get("author")):
        return jsonify(), 400

    if not PostDao.get_by_id(data.get("post")):
        return jsonify(), 400

    try:
        commentSaved = CommentDao.insert(comment)
    except Exception:
        return jsonify(), 400

    return jsonify(commentSaved.__dict__), 201


@bp.put("/<int:id>")
def update_comment(id):
    data = request.get_json()
    comment = Comment(**data)
    comment.id = id
    try:
        comment_updated = CommentDao.update(comment)
    except Exception:
        return jsonify(), 400

    if not comment_updated:
        return abort(404, description="Not Found")

    return jsonify(comment_updated.__dict__), 200


@bp.delete("/<int:id>")
def delete_comment(id):
    CommentDao.delete_by_id(id)
    return jsonify(), 204
