from flask import Blueprint, jsonify, request

from microblog.dao import FollowerDao
from microblog.models import Follower

bp = Blueprint("followers", __name__, url_prefix="/api/followers")


@bp.get("/following/<int:id>")
def following_to(id):
    resp = FollowerDao.get_following_to(id)
    if resp == []:
        return jsonify(), 400

    return jsonify([f.__dict__ for f in resp]), 200


@bp.get("/followed/<int:id>")
def followed_by(id):
    resp = FollowerDao.get_followed_by(id)
    if resp == []:
        return jsonify(), 400

    return jsonify([f.__dict__ for f in resp]), 200


@bp.post("/")
def insert():
    data = request.get_json()
    follower = None
    try:
        follower = Follower(**data)
        follower_saved = FollowerDao.insert(follower)
    except Exception:
        return jsonify(), 400

    return jsonify(follower_saved.__dict__), 201


@bp.get("/<int:id>")
def detail(id):
    resp = FollowerDao.get_by_id(id)
    if resp is None:
        return jsonify(), 404
    return jsonify(resp.__dict__), 200


@bp.delete("/<int:id>")
def delete(id):
    try:
        FollowerDao.delete_by_id(id)
    except Exception:
        return jsonify(), 400

    return jsonify(), 204
