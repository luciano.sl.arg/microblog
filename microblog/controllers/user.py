from flask import Blueprint, abort, jsonify, request

from microblog.dao import UserDao
from microblog.models import User

bp = Blueprint("users", __name__, url_prefix="/api/users")


@bp.get("/<int:id>")
def get_user_by_id(id):
    user = UserDao.get_by_id(id)
    if not user:
        return abort(404, description="Not Found")
    return jsonify(user.__dict__), 200


@bp.get("/")
def list_users():
    return jsonify([u.__dict__ for u in UserDao.get_all()]), 200


@bp.post("/")
def create_user():
    data = request.get_json()
    user = None
    try:
        user = User(**data)
        user_saved = UserDao.insert(user)
    except Exception:
        return jsonify(), 400

    return jsonify(user_saved.__dict__), 201


@bp.put("/<int:id>")
def update_user(id):
    data = request.get_json()
    user = User(**data)
    user.id = id
    try:
        user_updated = UserDao.update(user)
    except Exception:
        return jsonify(), 400

    if not user_updated:
        return abort(404, description="Not Found")

    return jsonify(user_updated.__dict__), 200


@bp.delete("/<int:id>")
def delete_user(id):
    UserDao.delete_by_id(id)
    return jsonify(), 204
