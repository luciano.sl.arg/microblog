from flask import Blueprint, abort, jsonify, request

from microblog.dao import PostDao, UserDao
from microblog.models import Post

bp = Blueprint("posts", __name__, url_prefix="/api/posts")


@bp.get("/<int:id>")
def get_post_by_id(id):
    post = PostDao.get_by_id(id)
    if not post:
        return abort(404, description="Not Found")
    return jsonify(post.__dict__), 200


@bp.get("/")
def list_posts():
    return jsonify([x.__dict__ for x in PostDao.get_all()]), 200


@bp.post("/")
def create_post():
    data = request.get_json()
    if not UserDao.get_by_id(data.get("author")):
        return jsonify(), 400

    try:
        post = Post(**data)
        postSaved = PostDao.insert(post)
    except Exception:
        return jsonify(), 400

    return jsonify(postSaved.__dict__), 201


@bp.put("/<int:id>")
def update_post(id):
    data = request.get_json()
    post = Post(**data)
    post.id = id
    try:
        post_updated = PostDao.update(post)
    except Exception:
        return jsonify(), 400

    if not post_updated:
        return abort(404, description="Not Found")

    return jsonify(post_updated.__dict__), 200


@bp.delete("/<int:id>")
def delete_post(id):
    try:
        PostDao.delete_by_id(id)
    except Exception:
        return jsonify(), 400

    return jsonify(), 204
