import sqlite3

from microblog.config import DATABASE
from microblog.models import Comment


def comment_factory(cursor, row):
    comment = Comment(row[1], row[2], row[3])
    comment.updated = row[4]
    comment.created = row[5]
    comment.id = row[0]
    return comment


class CommentDao:
    @classmethod
    def get_all(cls):
        con = sqlite3.connect(DATABASE)
        con.row_factory = comment_factory
        cur = con.cursor()
        try:
            cur.execute("SELECT * FROM comments")
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchall()

    @classmethod
    def get_by_id(cls, _id):
        if not _id:
            return None
        con = sqlite3.connect(DATABASE)
        con.row_factory = comment_factory
        cur = con.cursor()
        try:
            cur.execute("SELECT * FROM comments WHERE id=?", (_id,))
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchone()

    @classmethod
    def insert(cls, comment):
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            cur.execute(
                """INSERT INTO comments
                    (author, post, content)
                    VALUES (?, ?, ?)""",
                (comment.author, comment.post, comment.content),
            )
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()

        return cls.get_by_id(cur.lastrowid)

    @classmethod
    def update(cls, comment):
        con = sqlite3.connect(DATABASE)
        con.row_factory = comment_factory
        cur = con.cursor()
        try:
            cur.execute(
                "UPDATE comments SET content=? WHERE id=?",
                (comment.content, comment.id),
            )
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()
        return cls.get_by_id(comment.id)

    @classmethod
    def delete_by_id(cls, _id):
        con = sqlite3.connect(DATABASE)
        con.row_factory = comment_factory
        cur = con.cursor()
        try:
            cur.execute("DELETE FROM comments WHERE id=?", (_id,))
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex

        finally:
            con.close()
