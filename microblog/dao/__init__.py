from microblog.dao.comment import CommentDao
from microblog.dao.follower import FollowerDao
from microblog.dao.post import PostDao
from microblog.dao.user import UserDao
