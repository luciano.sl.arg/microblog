import sqlite3

from microblog.config import DATABASE
from microblog.models import Follower


def follower_factory(cursor, row):
    follower = Follower(row[1], row[2])
    follower.updated = row[3]
    follower.created = row[4]
    follower.id = row[0]
    return follower


class FollowerDao:
    @classmethod
    def get_by_id(cls, user_id):
        con = sqlite3.connect(DATABASE)
        con.row_factory = follower_factory
        cur = con.cursor()
        try:
            cur.execute(
                """
                SELECT
                    id,
                    following,
                    followed,
                    updated,
                    created
                FROM followers
                WHERE id=?""",
                (user_id,),
            )
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchone()

    @classmethod
    def get_following_to(cls, user_id):
        con = sqlite3.connect(DATABASE)
        con.row_factory = follower_factory
        cur = con.cursor()
        try:
            cur.execute(
                """
                SELECT
                    id,
                    following,
                    followed,
                    updated,
                    created
                FROM followers
                WHERE following=?""",
                (user_id,),
            )
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchall()

    @classmethod
    def get_followed_by(cls, user_id):
        con = sqlite3.connect(DATABASE)
        con.row_factory = follower_factory
        cur = con.cursor()
        try:
            cur.execute(
                """
                SELECT
                    id,
                    following,
                    followed,
                    updated,
                    created
                FROM followers
                WHERE followed=?
                """,
                (user_id,),
            )
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchall()

    @classmethod
    def get_follower_by_id(cls, _id):
        con = sqlite3.connect(DATABASE)
        con.row_factory = follower_factory
        cur = con.cursor()
        try:
            cur.execute(
                """
                SELECT
                    id,
                    following,
                    followed,
                    updated,
                    created
                FROM followers
                WHERE id=?
                """,
                (_id,),
            )
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchone()

    @classmethod
    def insert(cls, follower):
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            cur.execute(
                """
                INSERT INTO followers (following,followed)
                VALUES (?,?)""",
                (follower.following, follower.followed),
            )
            con.commit()
            follower.id = cur.lastrowid
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()

        return follower

    @classmethod
    def delete_by_id(cls, _id):
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            cur.execute(
                """
                DELETE FROM followers
                WHERE id=?""",
                (_id,),
            )
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()
