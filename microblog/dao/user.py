import sqlite3

from microblog.config import DATABASE
from microblog.models import User


def user_factory(cursor, row):
    user = User(
        firstname=row[1],
        secondname=row[2],
        lastname=row[3],
        birth=row[4],
        address=row[5],
        city=row[6],
        phone=row[7],
        email=row[8],
        password=row[9],
    )

    user.updated = row[10]
    user.created = row[11]
    user.id = row[0]
    return user


class UserDao:
    @classmethod
    def get_all(cls):
        con = sqlite3.connect(DATABASE)
        con.row_factory = user_factory
        cur = con.cursor()
        try:
            cur.execute("SELECT * FROM users")
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchall()

    @classmethod
    def get_by_id(cls, _id):
        if not _id:
            return None

        con = sqlite3.connect(DATABASE)
        con.row_factory = user_factory
        cur = con.cursor()
        try:
            cur.execute("SELECT * FROM users WHERE id=?", (_id,))
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchone()

    @classmethod
    def insert(cls, user):
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            cur.execute(
                """INSERT INTO users (
                    firstname,
                    secondname,
                    lastname,
                    birth,
                    address,
                    city,
                    phone,
                    email,
                    password)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                (
                    user.firstname,
                    user.secondname,
                    user.lastname,
                    user.birth,
                    user.address,
                    user.city,
                    user.phone,
                    user.email,
                    user.password,
                ),
            )
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()

        return cls.get_by_id(cur.lastrowid)

    @classmethod
    def update(cls, user):
        con = sqlite3.connect(DATABASE)
        con.row_factory = user_factory
        cur = con.cursor()
        try:
            cur.execute(
                """UPDATE users SET
                firstname=?,
                secondname=?,
                lastname=?,
                birth=?,
                address=?,
                city=?,
                phone=?,
                email=?,
                password=?
                WHERE id=?""",
                (
                    user.firstname,
                    user.secondname,
                    user.lastname,
                    user.birth,
                    user.address,
                    user.city,
                    user.phone,
                    user.email,
                    user.password,
                    user.id,
                ),
            )
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()

        return cls.get_by_id(user.id)

    @classmethod
    def delete_by_id(cls, _id):
        con = sqlite3.connect(DATABASE)
        con.row_factory = user_factory
        cur = con.cursor()
        try:
            cur.execute("DELETE FROM users WHERE id=?", (_id,))
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()
