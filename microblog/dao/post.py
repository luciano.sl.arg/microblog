import sqlite3

from microblog.config import DATABASE
from microblog.models import Post


def post_factory(cursor, row):
    post = Post(row[1], row[2])
    post.updated = row[3]
    post.created = row[4]
    post.id = row[0]
    return post


class PostDao:
    @classmethod
    def get_all(cls):
        con = sqlite3.connect(DATABASE)
        con.row_factory = post_factory
        cur = con.cursor()
        try:
            cur.execute("SELECT * FROM posts")
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchall()

    @classmethod
    def get_by_id(cls, _id):
        if not _id:
            return None
        con = sqlite3.connect(DATABASE)
        con.row_factory = post_factory
        cur = con.cursor()
        try:
            cur.execute("SELECT * FROM posts WHERE id=?", (_id,))
        except sqlite3.Error as ex:
            raise ex

        return cur.fetchone()

    @classmethod
    def insert(cls, post):
        con = sqlite3.connect(DATABASE)
        cur = con.cursor()
        try:
            cur.execute(
                "INSERT INTO posts (author, content) VALUES (?, ?)",
                (post.author, post.content),
            )
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()

        return cls.get_by_id(cur.lastrowid)

    @classmethod
    def update(cls, post):
        con = sqlite3.connect(DATABASE)
        con.row_factory = post_factory
        cur = con.cursor()
        try:
            cur.execute(
                "UPDATE posts SET content=? WHERE id=?",
                (post.content, post.id),
            )
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()

        return cls.get_by_id(post.id)

    @classmethod
    def delete_by_id(cls, _id):
        con = sqlite3.connect(DATABASE)
        con.row_factory = post_factory
        cur = con.cursor()
        try:
            cur.execute("DELETE FROM posts WHERE id=?", (_id,))
            con.commit()
        except sqlite3.Error as ex:
            con.rollback()
            raise ex
        finally:
            con.close()
