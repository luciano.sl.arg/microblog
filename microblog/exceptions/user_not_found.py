from microblog.exeptions import NotFoundException


class UserNotFoundException(NotFoundException):
    def __init__(self, _id):
        super().__init__(f"id={_id} not found")
