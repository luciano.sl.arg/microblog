PRAGMA journal_mode = wal;
PRAGMA synchornous = normal;
PRAGMA analysis_limit = 400;
PRAGMA optimize;
PRAGMA recursive_triggers=OFF;
PRAGMA foreign_keys = ON;
PRAGMA foreign_key_check;

CREATE TABLE users (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    firstname       VARCHAR(30)  NOT NULL,
    secondname      VARCHAR(30),
    lastname        VARCHAR(30)  NOT NULL,
    birth           DATE         NOT NULL,
    address         VARCHAR(30)  NOT NULL,
    city            VARCHAR(30)  NOT NULL,
    phone           LONG         NOT NULL UNIQUE,
    email           VARCHAR(30)  NOT NULL UNIQUE,
    password        VARCHAR(255) NOT NULL,
    updated         DATETIME,
    created         DATETIME DEFAULT CURRENT_TIMESTAMP
    );


CREATE TABLE posts (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    author          INTEGER,
    content         VARCHAR(255) NOT NULL,
    updated         DATETIME,
    created         DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (author) REFERENCES users(id) ON DELETE CASCADE
    );

CREATE TABLE comments (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    author          INTEGER,
    post            INTEGER,
    content         VARCHAR(255) NOT NULL,
    updated         DATETIME,
    created         DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (author) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (post) REFERENCES posts(id) ON DELETE CASCADE
    );

CREATE TABLE followers (
    id              INTEGER PRIMARY KEY AUTOINCREMENT,
    following       INTEGER,
    followed        INTEGER,
    updated         DATETIME,
    created         DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (followed) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (following) REFERENCES users(id) ON DELETE CASCADE,
    UNIQUE (followed,following)
    );

CREATE TRIGGER users_update AFTER UPDATE ON users
FOR EACH ROW
BEGIN
    UPDATE users SET updated=CURRENT_TIMESTAMP WHERE id=OLD.id;
END;

CREATE TRIGGER posts_update AFTER UPDATE ON posts
FOR EACH ROW
BEGIN
    UPDATE posts SET updated=CURRENT_TIMESTAMP WHERE id=OLD.id;
END;

CREATE TRIGGER comments_update AFTER UPDATE ON comments
FOR EACH ROW
BEGIN
    UPDATE comments SET updated=CURRENT_TIMESTAMP WHERE id=OLD.id;
END;

CREATE TRIGGER followers_update AFTER UPDATE ON followers
FOR EACH ROW
BEGIN
    UPDATE followers SET updated=CURRENT_TIMESTAMP WHERE id=OLD.id;
END;
/*
CREATE TRIGGER users_insert AFTER INSERT ON users FOR EACH ROW
BEGIN
    UPDATE users SET created=DATETIME() WHERE users.id=NEW.id;
END;
*/
